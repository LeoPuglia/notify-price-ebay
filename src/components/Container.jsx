import React from 'react'
import '../styles/container.css'


export default props => {
    return (
        <div className='container'>
            <h1>Receba as promoções do ebay</h1>
            {props.children}
        </div>
    )
}