import React from 'react'
import _ from 'lodash'

export default props => (
    <select {...props.select} className='form-control' >
        <option selected disabled>Receber notificação a cada :</option>
        <option value={2}>2 Minutos</option>
        <option value={10}>10 Minutos</option>
        <option value={30}>30 Minutos</option>
    </select>
)

