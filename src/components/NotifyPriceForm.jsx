import React, { Component } from 'react'
import { reduxForm, Field, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { insertNotifyPriceAPI } from '../models/notifyPriceService'
import Input from '../components/Input'
import Select from '../components/Select'

class NotifyPriceForm extends Component{
    render(){
        return(
            <form role='form' onSubmit={insertNotifyPriceAPI}>
                <div className='box-body'>
                    <Field name='search' component={Input} label='Busca' cols='12 4' 
                        placeholder='Informe o nome de um produto ...' />

                    <Field name='email' component={Input} label='Email' cols='12 4' 
                        placeholder='Informe seu email para receber notificações ...' />
                    
                    <Field name='period' component={Select} label='Periodo' cols='12 4' 
                        placeholder='Escolha o periodo que deseja receber as notificações ...' />
                    
                    <button type='submit'>
                        Enviar notificações
                    </button>
                </div>
            </form>
        )
        
    }
}




NotifyPriceForm = reduxForm({form: 'notifyPriceForm', destroyOnUnmount: false})(NotifyPriceForm)
const selector = formValueSelector('notifyPriceForm')
const mapStateToProps = state => ({ 
        search: selector(state, 'search'), 
        email: selector(state, 'email') , 
        period: selector(state, 'email') 
    })
//const mapDispatchToProps = dispatch => bindActionCreators({init}, dispatch)
export default connect(mapStateToProps, null)(NotifyPriceForm)