import React , {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { insertNotifyPriceAPI } from '../models/notifyPriceService'
import NotifyPriceForm from './NotifyPriceForm'
import Container from './Container'
import '../styles/main.css'


class NotifyPrice extends Component{
    // constructor(props){
    //     super(props)
    //     this.state = {search: '', email:'', period:0}
    // }
    render(){
        return(
            <Container>
                <NotifyPriceForm onSubmit={insertNotifyPriceAPI}  />
            </Container>
        )
    }
}



const mapDispatchToProps = dispatch => bindActionCreators({insertNotifyPriceAPI}, dispatch)
export default connect(null, mapDispatchToProps)(NotifyPrice)