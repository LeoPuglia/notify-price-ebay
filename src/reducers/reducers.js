import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import NotifyPriceReducer from './notifyPriceReducer'

const rootReducer = combineReducers({
    notifyPrice: NotifyPriceReducer,
    form: formReducer,
    toastr: toastrReducer
})

export default rootReducer