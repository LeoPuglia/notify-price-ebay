const INITIAL_STATE = {summary: {search: '', email: '', period:0}}

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'NOTIFY_CREATED':
            return { ...state, summary: action.payload.data }
        default:
            return state
    }
}