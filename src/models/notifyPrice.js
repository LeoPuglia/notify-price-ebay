//const restful = require('node-restful')
//const mongoose = restful.mongoose
//const mongoose = require('../configs/database')
const mongoose = require('mongoose')

const notifyPriceSchema = new mongoose.Schema({
    search: {type: String, required: true},
    userEmail: {type: String, required: true},
    periodOfSendEmail: {type: Number, required: true , enum:[2,10,30]}
})

module.exports = mongoose.model('NotifyPrice',notifyPriceSchema)



// const notifyPriceSchema = mongoose.model({
//     search: {type: String, required: true},
//     userEmail: {type: String, required: true},
//     periodOfSendEmail: {type: Number, required: true , enum:[2,10,30]}
// })

