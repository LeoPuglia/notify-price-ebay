const _ = require('lodash')
const NotifyPrice = require('./notifyPrice')

const emailRegex = /\S+@\S+\.\S+/

//#region Private

const errorsHandler = ( errorsFromBase) => {
    const errors = []

    _.forIn(errorsFromBase, error => errors.push(error.message))

    return errors
}

const prepareNotifyPriceEntity = (req) => {
    const search = req.body.search || ''
    const email = req.body.email || ''
    const period = req.body.period || ''
    
    if( !search || !email || !period )
        return null
    
    if(!email.match(emailRegex))
        return null
    
    return new NotifyPrice({search , userEmail : email , periodOfSendEmail: period})
}

// const searchNotifyPrice = (notifyPrice) => {
//     if(notifyPrice === undefined || notifyPrice === null)
//         return null

//     return notifyPrice.find((err , notifyPrice) => {
//         if(err)
//             return null
//         else
//             return notifyPrice
//     })
// }

//#endregion


//#region GRUD
const insertNotifyPriceAPI = ({search ,email ,period }) => {
    
    console.log(`search: ${search }, email: ${email}, period: ${period}`)
    const newNotifyPrice = prepareNotifyPriceEntity({search,email , period});
    
    if( newNotifyPrice === null )
        return ""

    newNotifyPrice.save((err) =>{
        if(err)
            return errorsHandler( err)
        else
            return 'Sucesso ! Em breve você receberá as notificações!'
    })

}

// const updateNotifyPriceAPI = (res , req, next)  => {
//     const notifyPrice = prepareNotifyPriceEntity(req);
    
//     if( notifyPrice === null )
//         return res.status(400).send({errors: []})
    
//     const newNotifyPrice = () => searchNotifyPrice(notifyPrice);

//     if(!newNotifyPrice)
//         return res.status(400).send({errors: []})

//     newNotifyPrice.update((err) =>{
//         if(err)
//             return errorsHandler(res , err)
//         else
//             return res.status(200).send({message: 'Sucesso ! Em breve você receberá as notificações!'})
//     })
    
// }

// const searchNotifyPriceAPI = (res , req, next) => {

//     const notifyPrice = prepareNotifyPriceEntity(req);
    
//     if( notifyPrice === null )
//         return res.status(400).send({errors: []})

//     const newNotifyPrice = () => searchNotifyPrice(notifyPrice);
   
//     newNotifyPrice.find((err) =>{
//         if(err)
//             return errorsHandler(res , err)
//         else
//             return res.status(200).send({message: 'Sucesso ! Em breve você receberá as notificações!'})
//     })
// }



// const deleteNotifyPriceAPI = (res , req, next) => {
//     const notifyPrice = prepareNotifyPriceEntity(req);
    
//     if( notifyPrice === null )
//         return res.status(400).send({errors: []})
    
//     const newNotifyPrice = () => searchNotifyPrice(notifyPrice);
    
//     newNotifyPrice.delete((err) =>{
//         if(err)
//             return errorsHandler(res , err)
//         else
//             return res.status(200).send({message: 'Sucesso ! Em breve você receberá as notificações!'})
//     })
// }

//#endregion




// const sendEmail = (notify) => {
    
// }

module.exports = {insertNotifyPriceAPI}