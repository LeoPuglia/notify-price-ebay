const axios = require('axios')
const {URL_EBAY , CLIENT_ID,CLIENT_SECRET, OAUTH2_HOST} = require('../utils/const')

//import { getAuthorizationCode } from '../../utils/oAuth2Client'
const oauth = require('axios-oauth-client');

const getAuthorizationCode = oauth.client(axios.create(), {
    url: OAUTH2_HOST,
    grant_type: 'authorization_code',
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    redirect_uri: '',
    code: '',
    scope: '',
});

const APIBase = axios.create({
  baseURL: URL_EBAY,
  headers: {'Authorization': 'basic '+ getAuthorizationCode()}
})
 
const getItemFromEbay = (searchT) => {
    const search = searchT || ''

    return APIBase.get(`/item_summary/search?q=${search}`)
            .then(resp => {return resp.data })
}


const SendEmailToNotificatePrice = () => {
    const response = APIBase.get(`/item_summary/search?q=${search}`)
    .then(resp => {return resp.data })
}

// const getItemFromEbay = (search) => {
//     if(!search)
//         return;
    
//     const auth = getAuthorizationCode()

//     return axios.get(`${URL_EBAY}/item_summary/${search}?`)
//             .then(resp => (
//                     {
//                         type: 'PRICE_SEARCHED', 
//                         payload:resp.data
//                     }))
    
// }


module.exports = { getItemFromEbay }