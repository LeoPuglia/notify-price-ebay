export default {
    URL_EBAY: 'https://api.ebay.com/buy/browse/v1/',
    URL_EBAY_SANDBOX: 'https://api.sandbox.ebay.com',
    CLIENT_ID: '',
    CLIENT_SECRET: '',
    OAUTH2_HOST: ''

}
